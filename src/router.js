import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from './components/Home'
import Services from './components/Services'
import Projects from './components/Projects'
import HouseInClonskeagh from './components/projects/HouseInClonskeagh'
import VictorianDoors from './components/projects/VictorianDoors'
import Wallpaper from './components/projects/Wallpaper'

const router = new VueRouter({
  history: true
})

router.map({
  '/': {
    name: 'home',
    component: Home
  },
  '/#services': {
    name: 'services',
    component: Services
  },
  '/#about': {
    name: 'about',
    component: Home
  },
  '/projects': {
    name: 'projects',
    component: Projects
  },
  '/projects/house-in-clonskeagh': {
    name: 'house-in-clonskeagh',
    component: HouseInClonskeagh
  },
  '/projects/victorian-doors': {
    name: 'victorian-doors',
    component: VictorianDoors
  },
  '/projects/wallpaper': {
    name: 'wallpaper',
    component: Wallpaper
  }
})
  // For every new route scroll to the top of the page
router.beforeEach(function() {
  window.scrollTo(0, 0)
})
router.redirect({
  '*': '/'
})
export default router
