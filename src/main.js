import Vue from 'vue'
import VueMoment from 'vue-moment'
Vue.use(VueMoment)
import WOW from 'wow.js'
var ww = new WOW()
ww.init()
import App from './App'
import router from './router'
router.start(App, '#app')
