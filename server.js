import express from 'express'
import mongoose from 'mongoose'

let app = express()
app.use(express.static('dist'))

var uri = 'mongodb://mongo4.mydevil.net:27017/mo1338_rgrjs'
var options = {
  user: 'mo1338_rgrjs',
  pass: 'LORD00system'
}
mongoose.connect(uri, options)
let db
mongoose.connection.on('error', console.error.bind(console, 'connection error:'))
mongoose.connection.once('open', function(callback) {
  db = mongoose.connection
  app.listen(3000, () => console.log('Listening on port 3000'))
})

app.get('/data/links', (req, res) => {
  db.collection('links').find({}).toArray((err, links) => {
    if (err) {
      throw err
    }
    res.json(links)
  })
})
